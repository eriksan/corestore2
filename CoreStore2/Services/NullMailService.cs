﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStore2.Services
{
    /*This class is just a test dummy for logging messages that are supposed to be emailed in the real one.*/
    public class NullMailService : IMailService
    {
        private readonly ILogger<NullMailService> _logger;

        //We can inject a logger into our class that we can use to log information, no matter where it is going.
        public NullMailService(ILogger<NullMailService> logger)
        {
            this._logger = logger;
        }


        public void SendMessage(string to, string subject, string body)
        {
            //Log the message
            _logger.LogInformation($"To: {to} Subject: {subject} Body: {body}");
        }
    }
}
