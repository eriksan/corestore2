﻿using System.ComponentModel.DataAnnotations;

namespace CoreStore2.ViewModels
{
    public class OrderItemViewModel
    {
        public int Id { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }

        /* Prefixing these with "Product" to take advantage of 
         * Automapper's convention which infers that we want
         * the name of a subobject. We want to flatten the products
         * into this viewmodel. In a lot of cases, especially 
         * fairly read-only, this is the way to handle it. It will
         * use this info to infer that we want possibly the name of 
         * a sub-object. 
         * */
        [Required]
        public int ProductId { get; set; }
        public string ProductCategory { get; set; }
        public string ProductSize { get; set; }
        public string ProductTitle { get; set; }
        public string ProductArtist { get; set; }
        public string ProductArtId { get; set; } //To construct image links.
    }
}