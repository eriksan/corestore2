﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CoreStore2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildTheHost(args).Run();
        }

        public static IWebHost BuildTheHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args) //Sets up a default config file but we're not going to use it.
                /* Building up our configuration manually: 
                 * Takes a delegate that is going to pass in a configuation builder so that 
                 * we can add our own configuration options. 
                 */
                .ConfigureAppConfiguration(SetupConfiguration) 
                .UseStartup<Startup>()
                .Build();

        private static void SetupConfiguration(WebHostBuilderContext ctx, IConfigurationBuilder builder)
        {
            //Removing the default configuration options.
            builder.Sources.Clear();

            /* The configuration in ASP.NET Core is flexible and able to use several different
             * types of configurations, not just Json. In this way, we may reuse existing con-
             * figuration, if they exist. builder allows us to add several different kinds of 
             * configs!
             * 
             * This is a hiearchy of configuration. If the same config exists in multiple places,
             * the latest (furthest down) configuration source will win. This allows for possibly 
             * different configs in development (using json file for example) and production 
             * (using environtment variables for example). This gives us the ability to set things
             * at dev time and allow production to change those vars without having to manage the
             * config file and storing real secrets.
             * */

            builder.AddJsonFile("config.json", false, true) //Config file is mandatory and reloads on change
                //.AddXmlFile("config.xml", true) //optional extra config files can be used, slick.
                .AddEnvironmentVariables();
        }
    }
}
