﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStore2.Data;
using CoreStore2.Services;
using CoreStore2.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoreStore2.Controllers
{
    public class AppController : Controller
    {
        /* Using dependency injection here...
         * Note how we're requiring the IMailService in the constructor.
         * This will lask the service layer to figure out how to construct a
         * mailService, which requires a logger. The services layer will resolve 
         * dependencies of dependencies. The services layer knows which mailService
         * class to use due to services.AddTransient<IMailService, NullMailService>();
         * in ConfigureServices() in Startup.cs. AppController never has to understand 
         * the tree of dependencies, the registration in Startup.cs will do the heavy
         * lifting.
         */
        private readonly IMailService _mailService;
        private readonly ICoreStore2Repository _repository;

        //Using a constructor to inject the services we need
        public AppController(IMailService mailService, ICoreStore2Repository repository)
        {
            this._mailService = mailService;
            this._repository = repository;
        }

        public IActionResult Index()
        {

            return View();
        }

        //We can specify a route specific to contact:
        [HttpGet("contact")]
        public IActionResult Contact()
        {
            //ViewBag.Title = "Contact us"; //We can use viewbag in the controller, but we can also put them in  :)
            //throw new InvalidOperationException("Bad things happen."); //500 error.
            return View();
        }

        [HttpPost("contact")]
        public IActionResult Contact(ContactViewModel model) //Accepting payload from page, a ContactViewModel we created in /ViewModels.
        {
            if (ModelState.IsValid)
            {
                //Send the email
                _mailService.SendMessage("erik.martinessanches@gmail.com", model.Subject, $"From: {model.Name} - {model.Email}, Message: {model.Message}");
                ViewBag.UserMessage = "Mail sent.";
                ModelState.Clear(); //Clear the form.
            }
            return View();
        }

        [HttpGet("about")]
        public IActionResult About()
        {
            ViewBag.Title = "About us";
            return View();
        }

        [Authorize]
        public IActionResult Shop()
        {
            var results = _repository.GetAllProducts();
            return View(results);
        }
    }
}