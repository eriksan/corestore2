﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStore2.Data;
using CoreStore2.Data.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CoreStore2.Controllers
{
    //[Produces("application/json")]
    [Route("api/[Controller]")]
    public class ProductsController : Controller
    {
        private readonly ICoreStore2Repository _repository;
        private readonly ILogger<ProductsController> _logger;

        public ProductsController(ICoreStore2Repository repository, ILogger<ProductsController> logger)
        {
            _repository = repository;
            _logger = logger;
        }
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_repository.GetAllProducts()); //We're wrapping in a 200 ok status code, tying method calls to status codes.
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get products: {ex}");
            }
            return BadRequest("Failed to get products."); //Status 400 I believe...
        }
    }
}