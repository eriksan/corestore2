﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CoreStore2.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;

        public AccountController(ILogger<AccountController> logger)
        {
            _logger = logger;
        }

        
        public IActionResult Login()
        {
            if (this.User.Identity.IsAuthenticated) //If logging in while already logged in
            {
                return RedirectToAction("Index", "App"); //Redirect to the Index action on the App controller.
            }
            return View();
        }
    }
}