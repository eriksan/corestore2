﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CoreStore2.Data;
using CoreStore2.Data.Entities;
using CoreStore2.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CoreStore2.Controllers
{
    //[Produces("application/json")]
    [Route("api/[Controller]")]
    public class OrdersController : Controller
    {
        private readonly ICoreStore2Repository _repository;
        private readonly ILogger<OrdersController> _logger;
        private readonly IMapper _mapper;

        public OrdersController(ICoreStore2Repository repository, 
            ILogger<OrdersController> logger, 
            IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult Get(bool includeItems = true) //optional param to emable query string ?includeItems=false
        {
            try
            {
                var results = _repository.GetAllOrders(includeItems);
                //Below we're telling the in and out types when we call Map.
                return Ok(_mapper.Map<IEnumerable<Order>, IEnumerable<OrderViewModel>>(results));
            } catch (Exception ex)
            {
                _logger.LogError($"Failed to get orders: {ex}");
                return BadRequest("Failed to get orders.");
            }
        }
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var order = _repository.GetOrderById(id);
                //Map Order to OrderViewModel. We always want to be returning view models.
                if (order != null) return Ok(_mapper.Map<Order, OrderViewModel>(order)); 
                else return NotFound(); //If id is invalid, returning not found makes more sense than BadRequest() below.
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get orders: {ex}");
                return BadRequest("Failed to get orders.");
            }
        }
        [HttpPost]
        public IActionResult Post([FromBody]OrderViewModel model)
        {
            //add it to db
            try
            {
                if (ModelState.IsValid) //Similarly to when building the contact page.
                {
                    // Map from OrderViewModel to Order...
                    var newOrder = _mapper.Map<OrderViewModel, Order>(model);

                    // Doing some validation, in case they didn't specify the orderdate, 
                    // we'll use today.
                    if(newOrder.OrderDate == DateTime.MinValue)
                    {
                        newOrder.OrderDate = DateTime.Now;
                    }

                    _repository.AddEntity(newOrder);
                    if (_repository.SaveAll())
                    {
                        //Now we're going to use the Map in the opposite direction.
                        //Convert back from Order to OrderViewModel
                        return Created($"api/orders/{newOrder.Id}", _mapper.Map<Order, OrderViewModel>(newOrder)); //Returns 201 Created.
                    }
                }
                else
                {
                    return BadRequest(ModelState); //the actual errors about the data they sent us will be shown to them.
                }
                
            } catch (Exception ex)
            {
                _logger.LogError($"Failed to save a new order: {ex}");
            }
            return BadRequest("Failed to save new order.");
        }
    }
}