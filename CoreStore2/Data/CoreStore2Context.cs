﻿using CoreStore2.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStore2.Data
{
    /* DbContext knows how to execute queries to a data store.
     * In order to create these queryable endpoints we need to create
     * some properties that will allow us to get at these different 
     * entities. Thes properties will be of type DbSet<Type of entity>.
     * We will be able to issue queries against Products and Orders and
     * add them to the database.
     * 
     * Unless we want to query OrderItems across Orders, we don't actually
     * need to create a property for Orderitems. Thinking about the way 
     * we want to use our entities, we'll probably only want to consider
     * OrderItems as child of Orders. Usually we only want to create a 
     * DbSet for something we want to query against directly.
     */
    public class CoreStore2Context : IdentityDbContext<StoreUser>
    {
        /* We need to make this constructor.
         * When the context is being added, it's taking those options
         * we specifien in Startup.cs and passing them into the context 
         * so that it know what connection string to use. 
         * */

        public CoreStore2Context(DbContextOptions<CoreStore2Context> options): base(options)
        {

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}
