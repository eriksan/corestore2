﻿using System.Collections.Generic;
using CoreStore2.Data.Entities;

namespace CoreStore2.Data
{
    public interface ICoreStore2Repository
    {
        IEnumerable<Product> GetAllProducts();
        IEnumerable<Product> GetProductsByCategory(string category);
        
        IEnumerable<Order> GetAllOrders(bool includeItems); //to emable query string ?includeItems=false
        Order GetOrderById(int id);

        bool SaveAll();
        void AddEntity(object model);
    }
}