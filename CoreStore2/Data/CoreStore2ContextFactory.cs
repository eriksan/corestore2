﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStore2.Data
{
    public class CoreStore2ContextFactory : IDesignTimeDbContextFactory<CoreStore2Context>
    {
        public CoreStore2Context CreateDbContext(string[] args)
        {
            //Create a configuration
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("config.json")
                .Build();

            var bldr = new DbContextOptionsBuilder<CoreStore2Context>();
            bldr.UseSqlServer(config.GetConnectionString("CoreStore2ConnectionString"));
            return new CoreStore2Context(bldr.Options);
        }
    }
}
