﻿using CoreStore2.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStore2.Data
{
    public class CoreStore2Repository : ICoreStore2Repository
    {
        private readonly CoreStore2Context _ctx;
        private readonly ILogger<CoreStore2Repository> _logger;

        public CoreStore2Repository(CoreStore2Context ctx, ILogger<CoreStore2Repository> logger)
        {
            this._ctx = ctx;
            this._logger = logger;
        }

        public void AddEntity(object model)
        {
            _ctx.Add(model);
        }

        public IEnumerable<Order> GetAllOrders(bool includeItems)//to emable query string ?includeItems=false
        {
            if (includeItems)
            {
                return _ctx.Orders
                    .Include(o => o.Items) //Include items in the response
                    .ThenInclude(i => i.Product) //show product within items in orderitem...
                    .ToList();
            }
            else
            {
                return _ctx.Orders
                    .ToList();
            }
        }

        public IEnumerable<Product> GetAllProducts()
        {
            try
            {
                _logger.LogInformation("GetAllProducts() was called.");
                return _ctx.Products
                    .OrderBy(p => p.Title)
                    .ToList();
            }
            catch (Exception ex) //Consider catching exceptions in other methods too.
            {
                _logger.LogError($"Failed to get all products {ex}");
                return null;
            }
            
        }

        public Order GetOrderById(int id)
        {
            return _ctx.Orders
            .Include(o => o.Items) //Include items
            .ThenInclude(i => i.Product) //show product within items in orderitem...
            .Where(o => o.Id == id)
            .FirstOrDefault(); //Prevent from returning a list, returns a first result or a null if it didn't find it.
        }

        public IEnumerable<Product> GetProductsByCategory(string category)
        {
            return _ctx.Products
                .Where(p => p.Category == category)
                .ToList();
        }

        public bool SaveAll()
        {
            return _ctx.SaveChanges() > 0;
        }
    }
}
