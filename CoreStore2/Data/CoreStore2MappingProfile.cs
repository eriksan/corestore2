﻿using AutoMapper;
using CoreStore2.Data.Entities;
using CoreStore2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStore2.Data
{
    //This is in order to make Automapper work. 
    public class CoreStore2MappingProfile : Profile
    {
        public CoreStore2MappingProfile()
        {
            //Create map between two types:
            CreateMap<Order, OrderViewModel>()
                //When youre looking for an id, map it from the source id...
                .ForMember(o => o.OrderId, ex => ex.MapFrom(o => o.Id))
                .ReverseMap(); // Allows mapping in reverse, as set up in OrdersController/Post

            //A different situation, with two different mappings:
            CreateMap<OrderItem, OrderItemViewModel>()
                .ReverseMap();
        }
    }
}
