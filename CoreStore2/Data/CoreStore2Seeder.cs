﻿using CoreStore2.Data.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStore2.Data
{
    public class CoreStore2Seeder
    {
        private readonly CoreStore2Context _ctx;
        private readonly IHostingEnvironment _hosting;
        private readonly UserManager<StoreUser> _userManager;

        public CoreStore2Seeder(CoreStore2Context ctx, 
            IHostingEnvironment hosting,
            UserManager<StoreUser> userManager)
        {
            this._ctx = ctx;
            this._hosting = hosting;
            this._userManager = userManager;
        }

        //For filling the DB with example data. Will be called once when the project starts.
        public async Task Seed()
        {
            _ctx.Database.EnsureCreated(); //Ensure db exists.

            //Making a first user
            var user = await _userManager.FindByEmailAsync("erik.martinessanches@gmail.com");
            if (user == null)
            {
                user = new StoreUser()
                {
                    FirstName = "Erik",
                    LastName = "Martines Sanches",
                    UserName = "erik.martinessanches@gmail.com",
                    Email = "erik.martinessanches@gmail.com"
                };
            }

            //By default there are some rules on the password, hence: 
            var result = await _userManager.CreateAsync(user, "P@ssw0rd!");
            if (result != IdentityResult.Success)
            {
                throw new InvalidOperationException("Failed to create default user.");
            }

            if (!_ctx.Products.Any()) //True is there are not any products in db.
            {
                //Need to create sample data
                var filepath = Path.Combine(_hosting.ContentRootPath, "Data/art.json");
                var json = File.ReadAllText(filepath);
                //We need to give a type that we want to deserialize into:
                //Should return a list of products:
                var products = JsonConvert.DeserializeObject<IEnumerable<Product>>(json);
                _ctx.Products.AddRange(products); //Adding the large collection to Products using addRange.

                var order = new Order()
                {
                    OrderDate = DateTime.Now,
                    OrderNumber = "12345",
                    User = user,
                    Items = new List<OrderItem>()
                    {
                        new OrderItem()
                        {
                            Product = products.First(),
                            Quantity = 5,
                            UnitPrice = products.First().Price
                        }
                    }
                };
                _ctx.Orders.Add(order);
                _ctx.SaveChanges();
            }
        }
    }
}
