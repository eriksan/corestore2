﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreStore2.Data.Entities
{
  public class Order
  {
    public int Id { get; set; }
    public DateTime OrderDate { get; set; }
    public string OrderNumber { get; set; }
    //A one to many relationship will be created in the DB:
    public ICollection<OrderItem> Items { get; set; }
    //The user that owns this order: We're tying the identity
    //into the order system.
    public StoreUser User { get; set; }
    }
}
