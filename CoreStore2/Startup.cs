﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CoreStore2.Data;
using CoreStore2.Data.Entities;
using CoreStore2.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace CoreStore2
{
    public class Startup
    {
        //For getting configuration.
        private readonly IConfiguration _config;

        /* Allows us to inject certain basic interfaces that are setup in Program.cs. */
        public Startup(IConfiguration config)
        {
            this._config = config;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        { //ASP.NET Core requires us to use dependency injection (DI), therefore we need to:

            services.AddIdentity<StoreUser, IdentityRole>(cfg =>
            {
                cfg.User.RequireUniqueEmail = true;
            })
            .AddEntityFrameworkStores<CoreStore2Context>(); //Where to get the user data.

            //Requred for the app to work with a DB. Note that I have creted a CoreStore2Context in /Data.
            services.AddDbContext<CoreStore2Context>(cfg =>
            {
                //Passing a connection string using _config which has GetConnectionString
                //as a special property already to give values.
                cfg.UseSqlServer(_config.GetConnectionString("CoreStore2ConnectionString"));
            });

            services.AddAutoMapper();

            //Adding a dummy "email" service like this:
            //Here we tell the interface and the implementation of this service
            //so that a consumer of the IMailService shouldn't care about the impementation!
            //Add support for real mail service later byt switching out the NullMailService
            //for a real mail implementation.
            services.AddTransient<IMailService, NullMailService>(); //Transient (opposed to AddScoped which is keps around for the length of the request) is not kept around. AddSingleton is for services created once and kept for the lifetime of a server.
            
            //Adding our Seeder class to services.
            services.AddTransient<CoreStore2Seeder>();

            /* Registering the repository with the service layer. The scope is usually 
             * a request. Add ICoreStore2Repository as a service people can use, but use 
             * CoreStore2Repository as the implementation, in this version. This makes the 
             * implementation interchangeable as with the IMailService above. (In a testing 
             * project we could use a mock CoreStore2Repository that returns static data.)
             * */
            services.AddScoped<ICoreStore2Repository, CoreStore2Repository>();

            /* We have a self referencing loop in our data model. But in order to flatten
             * into a plain Json, we need to avoid the loop like this: */
            services.AddMvc()
                .AddJsonOptions(opt => opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore); //Trim off self-referencing objects from the JSON´sent back to client.

            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            //app.Run(async (context) =>
            //{
            //    await context.Response.WriteAsync("Hello World!");
            //});
            //app.UseDefaultFiles(); //We can comment this out because we're not serving html, we're always serving through MVC.
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            } else
            {
                app.UseExceptionHandler("/error"); //use this route for errors shown to users...
            }
            app.UseStaticFiles();

            app.UseAuthentication(); //Should come before MVC

            /*Now, configure MVC. */
            app.UseMvc(cfg => //Passing in a lambda to set up routes.
            {
                cfg.MapRoute("Default", "{controller}/{action}/{id?}", //If you see this URL (using placeholders here, but could use real names) after port and server name
                    new { controller = "App", Action = "Index" }); //then execute this action on this controller.
            });

            if (env.IsDevelopment())
            {
                //Seed db.
                //We will use a scope temporarily for these couple of commands.
                using (var scope = app.ApplicationServices.CreateScope())
                {
                    //ServiceProvider can create instances of services.
                    var seeder = scope.ServiceProvider.GetService<CoreStore2Seeder>();
                    seeder.Seed().Wait();
                }
            }
        }
    }
}
