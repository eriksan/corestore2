﻿//Wrapping everything in an anonymous func, 
//Executes it as soon as it is ready.
//This moves everything out of the global scope. 
$(document).ready(function () {
    //console.log("Hello World.");

    var theForm = $("#theForm"); //Select from document.
    theForm.hide();

    var button = $("#buyButton");
    button.on("click", function () { /*Create event listener in JQ.*/
        console.log("Buying item");
    });

    //Returns a collection due to class.
    var productInfo = $(".product-props li");//Get lis inside the product-props
    //Setting up events on a collection of items:
    productInfo.on("click", function () {
        //Jqueryfying this.innerText, <- which may not work on all browsers.
        console.log("You clicked on " + $(this).text());
    });

    /*Get the couple of objects to manipulate.*/
    var $loginToggle = $("#loginToggle");
    var $popupForm = $(".popup-form");

    $loginToggle.on("click", function () {
        $popupForm.slideToggle(50);
    });

});
